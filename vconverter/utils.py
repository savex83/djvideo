import os
from ffmpy.ffmpeg import FFmpeg

from django.conf import settings


def convert_file(input_file, title, compression='very_slow'):
    full_path, file_extension = os.path.splitext(input_file)
    if file_extension == '.mkv':
        output_filename = os.path.splitext(title)[0] + '.mp4'
        # print 'output_filename', output_filename
        # print 'input_file complete path', input_file
        # full_path_input = settings.VR1 + '/' + input_file
        full_path_output = settings.COMPRESSED + '/' + output_filename

        # print 'full_path_output', full_path_output
        # print '*'*10
        # return ''

        arguments = '-c:v libx264 -crf 28 -strict experimental'
        arguments = '-r 30 -vcodec h264 -preset %s -acodec aac -strict -2' % (compression)
        ff = FFmpeg(inputs={input_file : None}, outputs={full_path_output: arguments})
        ff.run()
        return full_path_output


import json
import datetime, time

from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.conf import settings
from django.shortcuts import render_to_response, redirect
from django.template.loader import get_template
from django.template import RequestContext, Context
from django.utils.translation import ugettext as _
from django.forms.models import model_to_dict
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from .utils import convert_file
from archive.utils import db_set_video


# url(r'^convert/video$', name='convert_video'),
def convert_video(request):
    resp = {'status':0,}

    if request.method == 'POST':
        post = request.POST
        fname = post.get('fname', '')
        fpath = post.get('fpath', '')
        print fname
        print fpath

        full_path_output = convert_file(fpath, fname)
        db_set_video(fpath, fname, full_path_output)


    return HttpResponse(json.dumps(resp), content_type="application/json")


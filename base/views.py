from django.http import HttpResponseRedirect, HttpResponse 
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext, Context
import json
from django.template.loader import get_template
from django.db.models import Q
from django.forms.models import model_to_dict
from django.contrib.auth import authenticate, login, logout
from django.core.paginator import Paginator, InvalidPage, EmptyPage


def home(request):
    
    return render_to_response('home.html', {'settings': settings}, context_instance=RequestContext(request))
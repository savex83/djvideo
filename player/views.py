from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.conf import settings

from archive.utils import db_get_video_list, db_get_compressed_video_list, db_get_video


#  url(r'^video/list$', name='render_video_list'),
def render_video_list(request):
    items = {}
    video_list = db_get_compressed_video_list(request)
    items['video_list'] = video_list

    return render_to_response('player_list.html', {'settings': settings, 'items':items}, context_instance=RequestContext(request))


#  url(r'^video/list$', name='render_player'),
def render_player(request, video_id):
    items = {}
    video = db_get_video(request, video_id)
    items['video'] = video

    return render_to_response('player.html', {'settings': settings, 'items':items}, context_instance=RequestContext(request))
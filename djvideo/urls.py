import django
from django.views.static import serve
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

import base.views, archive.views, vconverter.views, player.views

import vr 

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'djvideo.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),

    #oer il caricamento delle librerie
    (r'^css/(?P<path>.*)$', serve, {'document_root': settings.CSS_ROOT}),
    (r'^js/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.JS_ROOT}),
    (r'^img/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.IMG_ROOT}),
    (r'^static/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.STATIC_ROOT}),
    #(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    (r'^res/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.RES_ROOT}),

    (r'^streamer/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.COMPRESSED}),
    (r'^vr1/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.VR1}),

    url(r'^$', base.views.home, name='home'),

    #archive
    url(r'^manage/archive/$', archive.views.manage_archive, name='manage_archive'),
    url(r'^show/video/list/$', archive.views.show_public_video, name='show_public_video'),


    # video
    url(r'^video/list$', player.views.render_video_list, name='render_video_list'),
    url(r'^video/player/(?P<video_id>.*)/$', player.views.render_player, name='render_player'),

    #vconverter
    url(r'^convert/video$', vconverter.views.convert_video, name='convert_video'),
)


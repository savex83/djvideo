from django.conf import settings
import os

from vconverter.utils import convert_file
from .utils import check_video_exist, db_set_video, db_get_video_by_filename, db_get_video_by_title


def discover_video():
    for dirname, dirnames, filenames in os.walk(settings.VR1):
        # print path to all filenames.
        for filename in filenames:
            # print 'settings.COMPRESSED', settings.COMPRESSED
            # print 'dirname', dirname
            if dirname != settings.COMPRESSED:
                print(os.path.join(dirname, filename))
                complete_path = os.path.join(dirname, filename)
                print 'complete_path', complete_path
                # bisogna convertire solo i mkv

                video_f = db_get_video_by_title({}, filename[:-4])
                if not video_f:
                    db_set_video(complete_path, filename, '')
                    print 'db_set_video comp', complete_path, filename
            else:
                print 'passo'


# Controlla che non esista il filename
def discover_comp_video():
    for dirname, dirnames, filenames in os.walk(settings.COMPRESSED):
        # print path to all filenames.
        for filename in filenames:
            # print(os.path.join(dirname, filename))
            compressed_path = os.path.join(dirname, filename)
            print 'compressed_path', compressed_path

            video_f = db_get_video_by_title({}, filename[:-4])
            if video_f:
                video_f.compressed = compressed_path
                video_f.compressed_file_size = os.stat(compressed_path).st_size / 1000000
                video_f.save()
                print 'Video aggiornato'
                print 'Filename', filename
            else:
                db_set_video('', filename, compressed_path)
                print 'db_set_video comp', compressed_path, filename

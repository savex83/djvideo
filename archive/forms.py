from django import forms
from django.utils.translation import ugettext as _

from .models import Video

class VideoForm(forms.ModelForm):
    choices_visible = ((), ())
    choices_visible = ((), ())

    class Meta:
        model = Video
        fields = (
            'title',
            'visible',
            'private',
            )
        widgets = {
            'title': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': _("Titolo"),}),
        }

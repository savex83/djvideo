from django.core.management.base import BaseCommand, CommandError
from archive.batch import discover_comp_video


class Command(BaseCommand):
    help = 'Scan the video directory to discover files'

    def handle(self, *args, **options):
        discover_comp_video()
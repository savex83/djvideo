import hashlib, os
from django.db.models import Q

from .models import Video


# if compressed=True check if exists the compressed file
def check_video_exist(filename, compressed=False):
    if compressed:
        ret = Video.objects.filter(filename=filename).exclude(compressed='').exists()
    else:
        ret = Video.objects.filter(filename=filename).exists()
    return ret


def db_set_video(complete_path, filename, full_path_output):
    video_db = Video()
    video_db.filename = filename
    video_db.fullpath = complete_path
    video_db.title = filename[:-4]
    video_db.compressed = full_path_output
    video_db.compression_str = ''
    if full_path_output:
        video_db.compressed_file_size = os.stat(full_path_output).st_size / 1000000
    if complete_path:
        video_db.file_size = os.stat(complete_path).st_size / 1000000
        video_db.md5 = md5(complete_path)
    video_db.save()


def db_get_video_list(request):
    video_list = Video.objects.filter(visible=True)
    return video_list


def db_get_compressed_video_list(request):
    video_list = Video.objects.filter(Q(visible=True), ~Q(compressed=''))
    return video_list


def db_get_video(request, video_id):
    try:
        video = Video.objects.get(pk=video_id)
    except Video.DoesNotExist:
        return ''
    return video


# To use only in the batch
def db_get_video_by_filename(request, filename):
    try:
        video = Video.objects.get(filename=filename)
    except Video.DoesNotExist:
        return ''
    return video


# To use only in the batch
def db_get_video_by_title(request, title):
    try:
        print 'title', title
        video = Video.objects.get(title=title)
    except Video.DoesNotExist:
        return ''
    return video


def db_get_video_by_md5(request, file_path):
    try:
        v_md5 = md5(file_path)
        video = Video.objects.get(md5=v_md5)
    except Video.DoesNotExist:
        return ''
    return video


def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()
import json, os

from django.http import HttpResponseRedirect, HttpResponse 
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext, Context
from django.template.loader import get_template
from django.db.models import Q
from django.forms.models import model_to_dict
from django.contrib.auth import authenticate, login, logout
from django.core.paginator import Paginator, InvalidPage, EmptyPage

from djvideo import vr
from .utils import db_get_video, db_get_video_by_md5
from .models import Video
from .forms import VideoForm


# url(r'^manage/archive$', name='manage_archive'),
def manage_archive(request):
    directories = [settings.VR1]
    items = {'video_list' : [], 'file_name_list':[]}
    supported_ext = ['.mkv', '.avi', ]
    if request.method == 'GET':
        video_db = Video.objects.all()
        for video in video_db:
            file_stat = {'video':video}
            file_stat['vform'] = VideoForm(instance=video)
            items['video_list'].append(file_stat)

    if request.method == 'POST':
        resp = {'status': 0}
        post = request.POST
        video_id = post.get('video_id', 0)
        v_form = VideoForm(post)
        if v_form.is_valid():
            #salva dati
            video = db_get_video(request, video_id)
            v_form = VideoForm(post, instance=video)
            v_form.save()
            resp['status'] = 1
        else:
            # restituisce form
            video = db_get_video(request, video_id)
            file_stat = {'video':video}
            file_stat['vform'] = v_form
            items['video_list'].append(file_stat)

            html_template = get_template('archive_video_form.html')
            d = RequestContext(request, {'settings': settings, 'items':items})
            html_content = html_template.render(d)
            resp['html'] = html_content

        return HttpResponse(json.dumps(resp), content_type="application/json")

    return render_to_response('archive_files.html', {'settings': settings, 'items':items}, context_instance=RequestContext(request))


def manage_archive_old(request):
    directories = [settings.VR1]
    items = {'file_list' : [], 'file_name_list':[]}
    supported_ext = ['.mkv', '.avi', ]
    
    for path in directories:
        for dirName, subdirList, fileList in os.walk(path):
            #print('Found directory: %s' % dirName)
            for fname in fileList:
                #print('\t%s' % fname[-4:])
                if fname[-4:] in supported_ext:
                    print('\t%s' % fname)
                    full_path = dirName+'/'+fname
                    items['file_list'].append(full_path)

                    file_stat = {'name':fname,
                                 'size_mb': os.stat(full_path).st_size / 1000000,
                                 'full_path': full_path}

                    video = db_get_video_by_md5(request, full_path)
                    if video:
                        if video.compressed:
                            file_stat['compressed'] = True
                            file_stat['compressed_size'] = os.stat(video.compressed).st_size / 1000000
                        else:
                            file_stat['compressed'] = True
                        file_stat['vform'] = VideoForm(instance=video)

                    items['file_name_list'].append(file_stat)
    
    return render_to_response('archive_files.html', {'settings': settings, 'items':items}, context_instance=RequestContext(request))


# To show the public compressed video
def show_public_video(request):
    items = {'video_list':[]}
    videos = Video.objects.filter(Q(visible=True), ~Q(compressed=''))
    items['video_list'] = videos

    return render_to_response('video_list.html', {'settings': settings, 'items':items}, context_instance=RequestContext(request))

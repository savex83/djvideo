from django.db import models
from django.conf import settings


class Video(models.Model):
    id = models.AutoField(primary_key=True)
    filename = models.FileField(upload_to=settings.VR1)
    fullpath = models.TextField(max_length=1024, default='')
    compressed = models.FileField()
    compression_str = models.TextField(max_length=1024, default='')
    title = models.TextField(max_length=256, default='')
    file_size = models.IntegerField(default=0)
    compressed_file_size = models.IntegerField(default=0)
    # original file md5
    md5 = models.TextField(max_length=32, default='')
    visible = models.BooleanField(default=True)
    private = models.BooleanField(default=True)

